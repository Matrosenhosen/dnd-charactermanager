package gui;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Random;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSplitPane;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import org.eclipse.wb.swing.FocusTraversalOnArray;

import db_controll.DB_Controll;
import logic.Controller;
import model.CharacterClass;
import model.Charakter;
import model.Race;

import java.awt.Color;

public class CharacterCreator extends JFrame {

	private JPanel contentPane;
	private JTextField txtPlayerName;
	private JTextField txtCharacterName;
	private JTextField txtStrength;
	private JTextField txtDexterity;
	private JTextField txtConstitution;
	private JTextField txtIntelligence;
	private JTextField txtWisdom;
	private JTextField txtCharisma;
	private Charakter charakter = new Charakter();
	private Controller controll = new Controller(charakter);
	private DB_Controll db = new DB_Controll("127.0.0.1");

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {

		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					CharacterCreator frame = new CharacterCreator();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public CharacterCreator() {
		db.setup();
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1106, 794);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(175, 238, 238));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		CardLayout cardlayout = new CardLayout(0, 0);
		contentPane.setLayout(cardlayout);

		JPanel panel = new JPanel();
		panel.setBackground(new Color(175, 238, 238));
		panel.setForeground(new Color(192, 192, 192));
		contentPane.add(panel, "name_3500362366900");
		panel.setLayout(new BorderLayout(0, 0));

		JLabel lblCharakterdaten = new JLabel("Charakterdaten");
		lblCharakterdaten.setBackground(new Color(175, 238, 238));
		lblCharakterdaten.setFont(new Font("Harlow Solid Italic", Font.PLAIN, 50));
		lblCharakterdaten.setHorizontalAlignment(SwingConstants.CENTER);
		panel.add(lblCharakterdaten, BorderLayout.NORTH);

		JPanel panel_1 = new JPanel();
		panel_1.setBackground(new Color(175, 238, 238));
		panel.add(panel_1, BorderLayout.CENTER);
		panel_1.setLayout(new GridLayout(0, 2, 0, 0));

		JLabel lblSpielername = new JLabel("Spielername:");
		lblSpielername.setForeground(new Color(0, 0, 0));
		lblSpielername.setBackground(new Color(175, 238, 238));
		lblSpielername.setFont(new Font("Script MT Bold", Font.PLAIN, 35));
		panel_1.add(lblSpielername);

		txtPlayerName = new JTextField();
		txtPlayerName.setForeground(new Color(0, 0, 0));
		txtPlayerName.setBackground(new Color(255, 255, 255));
		txtPlayerName.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent arg0) {
				if (componentsNotEmpty(txtPlayerName.getParent()))
					System.out.println("Voll");
				else
					System.out.println("Leer");
			}
		});
		txtPlayerName.setFont(new Font("Papyrus", Font.BOLD, 30));
		panel_1.add(txtPlayerName);
		txtPlayerName.setColumns(10);
		
		JLabel lblCharaktername = new JLabel("Charaktername:");
		lblCharaktername.setFont(new Font("Script MT Bold", Font.PLAIN, 35));
		panel_1.add(lblCharaktername);

		txtCharacterName = new JTextField();
		txtCharacterName.setForeground(new Color(0, 0, 0));
		txtCharacterName.setBackground(new Color(255, 255, 255));
		txtCharacterName.setFont(new Font("Papyrus", Font.BOLD, 30));
		panel_1.add(txtCharacterName);
		txtCharacterName.setColumns(10);

		JLabel lblAlignment = new JLabel("Alignment:");
		lblAlignment.setFont(new Font("Script MT Bold", Font.PLAIN, 35));
		panel_1.add(lblAlignment);

		JSplitPane splitPane_1 = new JSplitPane();
		splitPane_1.setForeground(new Color(175, 238, 238));
		splitPane_1.setBackground(new Color(175, 238, 238));
		splitPane_1.setContinuousLayout(true);
		splitPane_1.setOrientation(JSplitPane.VERTICAL_SPLIT);
		splitPane_1.setEnabled(false);
		splitPane_1.setResizeWeight(0.5);
		panel_1.add(splitPane_1);

		JComboBox<String> comboBox = new JComboBox<String>();
		comboBox.setForeground(new Color(175, 238, 238));
		comboBox.setBackground(new Color(128, 128, 128));
		comboBox.setFont(new Font("Papyrus", Font.BOLD, 20));
		comboBox.setToolTipText("");
		comboBox.setModel(new DefaultComboBoxModel<String>(new String[] {"Lawful", "Chaotic", "Neutral" }));
		comboBox.setSelectedIndex(-1);
		splitPane_1.setLeftComponent(comboBox);

		JComboBox<String> comboBox_1 = new JComboBox<String>();
		comboBox_1.setForeground(new Color(175, 238, 238));
		comboBox_1.setBackground(new Color(128, 128, 128));
		comboBox_1.setFont(new Font("Papyrus", Font.BOLD, 20));
		comboBox_1.setModel(new DefaultComboBoxModel<String>(new String[] {"Good", "Evil", "Neutral" }));
		comboBox_1.setSelectedIndex(-1);
		splitPane_1.setRightComponent(comboBox_1);

		JLabel lblKlasse = new JLabel("Klasse:");
		lblKlasse.setFont(new Font("Script MT Bold", Font.PLAIN, 35));
		panel_1.add(lblKlasse);

		JComboBox<String> cbKlasse = new JComboBox<String>();
		cbKlasse.setForeground(new Color(175, 238, 238));
		cbKlasse.setBackground(new Color(128, 128, 128));
		cbKlasse.setFont(new Font("Papyrus", Font.BOLD, 20));
		cbKlasse.setModel(new DefaultComboBoxModel<String>(new String[] { "Barbar", "Barde", "Druide", "K\u00E4mpfer",
				"M\u00F6nch", "Paladin", "Waldl\u00E4ufer", "Dieb", "Zauberer" }));
		cbKlasse.setSelectedIndex(-1);
		panel_1.add(cbKlasse);

		JLabel lblRasse = new JLabel("Rasse:");
		lblRasse.setFont(new Font("Script MT Bold", Font.PLAIN, 35));
		panel_1.add(lblRasse);

		JComboBox<String> cbRasse = new JComboBox<String>();
		cbRasse.setForeground(new Color(175, 238, 238));
		cbRasse.setBackground(new Color(128, 128, 128));
		cbRasse.setFont(new Font("Papyrus", Font.BOLD, 20));
		cbRasse.setModel(new DefaultComboBoxModel<String>(new String[] { "Zwerg", "Elf", "Halbling", "Mensch", "Halbelf",
				"Halb-Ork", "Gnom", "Drachenblut", "Tieflinge" }));
		cbRasse.setSelectedIndex(-1);
		panel_1.add(cbRasse);

		JSplitPane splitPane = new JSplitPane();
		splitPane.setBackground(new Color(175, 238, 238));
		splitPane.setContinuousLayout(true);
		splitPane.setEnabled(false);
		splitPane.setResizeWeight(0.5);
		panel.add(splitPane, BorderLayout.SOUTH);

		JButton btnWeiter = new JButton("Weiter");
		btnWeiter.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				cardlayout.next(contentPane);
			}
		});
		btnWeiter.setEnabled(true); // change to false later
		btnWeiter.setFont(new Font("Tahoma", Font.PLAIN, 24));
		splitPane.setRightComponent(btnWeiter);

		JButton btnZurck = new JButton("Zur\u00FCck");
		btnZurck.setEnabled(false);
		btnZurck.setFont(new Font("Tahoma", Font.PLAIN, 24));
		splitPane.setLeftComponent(btnZurck);
		panel.setFocusTraversalPolicy(new FocusTraversalOnArray(new Component[] { txtPlayerName, txtCharacterName,
				comboBox, splitPane_1, comboBox_1, cbKlasse, cbRasse, lblCharakterdaten, panel_1, lblSpielername,
				lblCharaktername, lblAlignment, lblKlasse, lblRasse, splitPane, btnWeiter, btnZurck }));

		JPanel panel_2 = new JPanel();
		panel_2.setBackground(new Color(175, 238, 238));
		contentPane.add(panel_2, "name_678837342066500");
		panel_2.setLayout(new BorderLayout(0, 0));

		JLabel lblCharakterwerte = new JLabel("Charakterwerte");
		lblCharakterwerte.setHorizontalAlignment(SwingConstants.CENTER);
		lblCharakterwerte.setFont(new Font("Harlow Solid Italic", Font.ITALIC, 50));
		panel_2.add(lblCharakterwerte, BorderLayout.NORTH);

		JSplitPane splitPane_2 = new JSplitPane();
		splitPane_2.setResizeWeight(0.5);
		panel_2.add(splitPane_2, BorderLayout.SOUTH);

		JButton btnZurck_1 = new JButton("Zur\u00FCck");
		btnZurck_1.setFont(new Font("Tahoma", Font.PLAIN, 24));
		splitPane_2.setLeftComponent(btnZurck_1);
		btnZurck_1.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				cardlayout.first(contentPane);
			}
		});

		JButton btnWeiter_1 = new JButton("Weiter");
		btnWeiter_1.setFont(new Font("Tahoma", Font.PLAIN, 24));
		splitPane_2.setRightComponent(btnWeiter_1);
		btnWeiter_1.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				cardlayout.next(contentPane);
				String alignment = comboBox.getItemAt(comboBox.getSelectedIndex()) + comboBox_1.getItemAt(comboBox_1.getSelectedIndex());
				controll.generateCharacter(txtPlayerName.getText(),txtCharacterName.getText(),cbKlasse.getSelectedIndex(),cbRasse.getSelectedIndex(),Integer.parseInt(txtStrength.getText()),Integer.parseInt(txtDexterity.getText()),Integer.parseInt(txtConstitution.getText()),Integer.parseInt(txtIntelligence.getText()),Integer.parseInt(txtWisdom.getText()),Integer.parseInt(txtCharisma.getText()),alignment);
				controll.charakterEinfuegen();
			}
		});
		JPanel panel_4 = new JPanel();
		panel_4.setBackground(new Color(175, 238, 238));
		panel_2.add(panel_4, BorderLayout.CENTER);
		panel_4.setLayout(new GridLayout(3, 6, 0, 0));

		JLabel lblStrength = new JLabel("Strength");
		lblStrength.setFont(new Font("Script MT Bold", Font.PLAIN, 35));
		lblStrength.setHorizontalAlignment(SwingConstants.CENTER);
		panel_4.add(lblStrength);
		

		JLabel lblDexterity = new JLabel("Dexterity");
		lblDexterity.setFont(new Font("Script MT Bold", Font.PLAIN, 35));
		lblDexterity.setHorizontalAlignment(SwingConstants.CENTER);
		panel_4.add(lblDexterity);

		JLabel lblConstitution = new JLabel("Constitution");
		lblConstitution.setFont(new Font("Script MT Bold", Font.PLAIN, 35));
		lblConstitution.setHorizontalAlignment(SwingConstants.CENTER);
		panel_4.add(lblConstitution);

		JLabel lblIntelligence = new JLabel("Intelligence");
		lblIntelligence.setFont(new Font("Script MT Bold", Font.PLAIN, 35));
		lblIntelligence.setHorizontalAlignment(SwingConstants.CENTER);
		panel_4.add(lblIntelligence);

		JLabel lblWisdom = new JLabel("Wisdom");
		lblWisdom.setFont(new Font("Script MT Bold", Font.PLAIN, 35));
		lblWisdom.setHorizontalAlignment(SwingConstants.CENTER);
		panel_4.add(lblWisdom);

		JLabel lblCharisma = new JLabel("Charisma");
		lblCharisma.setFont(new Font("Script MT Bold", Font.PLAIN, 35));
		lblCharisma.setHorizontalAlignment(SwingConstants.CENTER);
		panel_4.add(lblCharisma);

		txtStrength = new JTextField();
		txtStrength.setBackground(new Color(128, 128, 128));
		txtStrength.setEnabled(false);
		txtStrength.setFont(new Font("Papyrus", Font.BOLD, 35));
		txtStrength.setHorizontalAlignment(SwingConstants.CENTER);
		panel_4.add(txtStrength);
		txtStrength.setColumns(10);

		txtDexterity = new JTextField();
		txtDexterity.setBackground(new Color(128, 128, 128));
		txtDexterity.setEnabled(false);
		txtDexterity.setFont(new Font("Papyrus", Font.BOLD, 35));
		txtDexterity.setHorizontalAlignment(SwingConstants.CENTER);
		panel_4.add(txtDexterity);
		txtDexterity.setColumns(10);

		txtConstitution = new JTextField();
		txtConstitution.setBackground(new Color(128, 128, 128));
		txtConstitution.setEnabled(false);
		txtConstitution.setFont(new Font("Papyrus", Font.BOLD, 35));
		txtConstitution.setHorizontalAlignment(SwingConstants.CENTER);
		panel_4.add(txtConstitution);
		txtConstitution.setColumns(10);

		txtIntelligence = new JTextField();
		txtIntelligence.setBackground(new Color(128, 128, 128));
		txtIntelligence.setEnabled(false);
		txtIntelligence.setFont(new Font("Papyrus", Font.BOLD, 35));
		txtIntelligence.setHorizontalAlignment(SwingConstants.CENTER);
		panel_4.add(txtIntelligence);
		txtIntelligence.setColumns(10);

		txtWisdom = new JTextField();
		txtWisdom.setBackground(new Color(128, 128, 128));
		txtWisdom.setEnabled(false);
		txtWisdom.setFont(new Font("Papyrus", Font.BOLD, 35));
		txtWisdom.setHorizontalAlignment(SwingConstants.CENTER);
		panel_4.add(txtWisdom);
		txtWisdom.setColumns(10);

		txtCharisma = new JTextField();
		txtCharisma.setBackground(new Color(128, 128, 128));
		txtCharisma.setEnabled(false);
		txtCharisma.setFont(new Font("Papyrus", Font.BOLD, 35));
		txtCharisma.setHorizontalAlignment(SwingConstants.CENTER);
		panel_4.add(txtCharisma);
		txtCharisma.setColumns(10);

		JButton btnRandomstrength = new JButton("Klick mich f\u00FCr St\u00E4rke!");
		btnRandomstrength.setFont(new Font("Tahoma", Font.PLAIN, 11));
		btnRandomstrength.setForeground(new Color(0, 0, 0));
		btnRandomstrength.setBackground(new Color(175, 238, 238));
		panel_4.add(btnRandomstrength);
		btnRandomstrength.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				txtStrength.setText(Integer.toString(Controller.randomStat()));
				charakter.setStr(Integer.parseInt(txtStrength.getText()));
			}
		});

		JButton btnRandomdexterity = new JButton("Klick mich f\u00FCr Geschicklichkeit!");
		btnRandomdexterity.setFont(new Font("Tahoma", Font.PLAIN, 11));
		btnRandomdexterity.setBackground(new Color(175, 238, 238));
		panel_4.add(btnRandomdexterity);
		btnRandomdexterity.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				txtDexterity.setText(Integer.toString(Controller.randomStat()));
				charakter.setDex(Integer.parseInt(txtDexterity.getText()));
			}
		});
		JButton btnRandomconstitution = new JButton("Klick mich f\u00FCr Konstitution!");
		btnRandomconstitution.setFont(new Font("Tahoma", Font.PLAIN, 11));
		btnRandomconstitution.setBackground(new Color(175, 238, 238));
		panel_4.add(btnRandomconstitution);
		btnRandomconstitution.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				txtConstitution.setText(Integer.toString(Controller.randomStat()));
				charakter.setCon(Integer.parseInt(txtConstitution.getText()));
			}
		});
		JButton btnRandomintelligence = new JButton("Klick mich f\u00FCr Intelligenz!");
		btnRandomintelligence.setFont(new Font("Tahoma", Font.PLAIN, 11));
		btnRandomintelligence.setBackground(new Color(175, 238, 238));
		panel_4.add(btnRandomintelligence);
		btnRandomintelligence.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				txtIntelligence.setText(Integer.toString(Controller.randomStat()));
				charakter.setInt(Integer.parseInt(txtIntelligence.getText()));
			}
		});
		JButton btnRandomwisdom = new JButton("Klick mich f\u00FCr Weisheit!");
		btnRandomwisdom.setFont(new Font("Tahoma", Font.PLAIN, 11));
		btnRandomwisdom.setBackground(new Color(175, 238, 238));
		panel_4.add(btnRandomwisdom);
		btnRandomwisdom.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				txtWisdom.setText(Integer.toString(Controller.randomStat()));
				charakter.setWis(Integer.parseInt(txtWisdom.getText()));
			}
		});
		JButton btnRandomcharisma = new JButton("Klick mich f\u00FCr Charisma!");
		btnRandomcharisma.setFont(new Font("Tahoma", Font.PLAIN, 11));
		btnRandomcharisma.setBackground(new Color(175, 238, 238));
		panel_4.add(btnRandomcharisma);
		btnRandomcharisma.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				txtCharisma.setText(Integer.toString(Controller.randomStat()));
				charakter.setCha(Integer.parseInt(txtCharisma.getText()));
			}
		});

		panel_4.setFocusTraversalPolicy(new FocusTraversalOnArray(new Component[] { txtStrength, txtDexterity,
				txtConstitution, txtIntelligence, txtWisdom, txtCharisma, lblStrength, lblDexterity, lblConstitution,
				lblIntelligence, lblWisdom, lblCharisma, btnRandomstrength, btnRandomdexterity, btnRandomconstitution,
				btnRandomintelligence, btnRandomwisdom, btnRandomcharisma }));

		JPanel panel_3 = new JPanel();
		panel_3.setBackground(new Color(175, 238, 238));
		contentPane.add(panel_3, "name_679345137279300");
		panel_3.setLayout(new BorderLayout(0, 0));

		JLabel lblNewLabel = new JLabel("Look now in the Datenbank alla. Thanks for travelling with the DB");
		lblNewLabel.setFont(new Font("Comic Sans MS", Font.PLAIN, 30));
		panel_3.add(lblNewLabel, BorderLayout.NORTH);
	}

	private boolean componentsNotEmpty(Container container) {
		Component[] components = container.getComponents();
		JTextField temptTextField;
		JSplitPane temptSplitPane = new JSplitPane();
		for (int i = 0; i < components.length; i++) {
			if (components[i].getClass().isInstance(new JTextField())) {
				temptTextField = (JTextField) components[i];
				if (temptTextField.getText().isEmpty()) {
					return false;
				} else if (components[i].getClass().isInstance(new JSplitPane())) {
					temptSplitPane = (JSplitPane) components[i];
					if (!componentsNotEmpty(temptSplitPane))
						return false;
				} else if (components[i].getClass().isInstance(new JComboBox<>())) {
					@SuppressWarnings("unchecked")
					JComboBox<String> temptComboBox = (JComboBox<String>) components[i];
					if (temptComboBox.getSelectedIndex() < 0)
						return false;
				}
			}
		}
		return true;
	}

}
