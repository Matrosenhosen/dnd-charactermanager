package model;

import java.util.Arrays;

public class CharacterClass {

	 private String name;
	 private int lvl;
	 private int hitdie;
	 private int hpMultiplier;
	 private int profMod;
	 private String profGear;
	 private boolean[] proficiencies = new boolean[24]; 
	 private String[][] features;
	 
	public CharacterClass(String name, int lvl, int hitdie, int hpMultiplier, int profMod, String profGear, boolean[] proficiencys,
			String[][] features) {
		super();
		this.name = name;
		this.lvl = lvl;
		this.hitdie = hitdie;
		this.hpMultiplier = hpMultiplier;
		this.profMod = profMod;
		this.profGear = profGear;
		this.proficiencies = proficiencys;
		this.features = features;
	}
	public CharacterClass(String name) {
		this.name = name;
	}
	

//---------------------------------------------------Getter-Setter---------------------------------------------------------------------------
	
	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public int getLvl() {
		return lvl;
	}


	public void setLvl(int lvl) {
		this.lvl = lvl;
	}


	public int getHitdie() {
		return hitdie;
	}


	public void setHitdie(int hitdie) {
		this.hitdie = hitdie;
	}


	public int getHpMultiplier() {
		return hpMultiplier;
	}


	public void setHpMultiplier(int hpMultiplier) {
		this.hpMultiplier = hpMultiplier;
	}


	public int getProfMod() {
		return profMod;
	}


	public void setProfMod(int profMod) {
		this.profMod = profMod;
	}


	public String getProfGear() {
		return profGear;
	}


	public void setProfGear(String profGear) {
		this.profGear = profGear;
	}


	public boolean[] getProficiencys() {
		return proficiencies;
	}


	public void setProficiencys(boolean[] proficiencys) {
		this.proficiencies = proficiencys;
	}


	public String[][] getFeatures() {
		return features;
	}


	public void setFeatures(String[][] features) {
		this.features = features;
	}

//----------------------------------------------------------------To-String--------------------------------------------------------------------------------

	@Override
	public String toString() {
		return "name" + name + "Class [lvl=" + lvl + ", hitdie=" + hitdie + ", hpMultiplier=" + hpMultiplier + ", profMod=" + profMod
				+ ", profGear=" + profGear + ", proficiencys=" + Arrays.toString(proficiencies)
				+ ", features=" /*+ featuresToString() */+ "]";
	}
	 
	private String featuresToString() {
		
		String featuresString = "( ";
		featuresString = featuresString + features[0][0] ;							// Startet die Trait Kette
		featuresString = featuresString + " , " + features[0][1] ;					// mit dem ersten Trait
		
		for (int i = 1; i < features.length; i++) {						// automatische Generation des rests
			
				featuresString = featuresString + " , " + features[i][0] ;			
				featuresString = featuresString + " , " + features[i][1] ;
		}
		featuresString = featuresString + " )";
		return featuresString;
		
	}
	
}
