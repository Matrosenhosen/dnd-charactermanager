package model;

import java.util.Arrays;

public class Race {
	
	private String name;
	private int speed;
	private String abScInName;
	private int abScInValue;
	private String languages;
	private String[][] traits;
	
	public Race(String name, int speed, String abScInName, int abScInValue, String languages, String[][] traits) {
		this.name = name;
		this.speed = speed;
		this.abScInName = abScInName;
		this.abScInValue = abScInValue;
		this.languages = languages;
		this.traits = traits;
	}
	public Race(String name) {
		this.name = name;
	}

//----------------------------------------------------------------Getter-Setter---------------------------------------------------------------

	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public int getSpeed() {
		return speed;
	}


	public void setSpeed(int speed) {
		this.speed = speed;
	}


	public String getAbScInName() {
		return abScInName;
	}


	public void setAbScInName(String abScInName) {
		this.abScInName = abScInName;
	}


	public int getAbScInValue() {
		return abScInValue;
	}


	public void setAbScInValue(int abScInValue) {
		this.abScInValue = abScInValue;
	}


	public String getLanguages() {
		return languages;
	}


	public void setLanguages(String languages) {
		this.languages = languages;
	}


	public String[][] getTraits() {
		return traits;
	}


	public void setTraits(String[][] traits) {
		this.traits = traits;
	}

//------------------------------------------------------To-String-------------------------------------------------------------------

	@Override
	public String toString() {
		return "Race [name=" + name + ", speed=" + speed + ", abScInName=" + abScInName + ", abScInValue=" + abScInValue
				+ ", languages=" + languages + ", traits" /*+ traitsToString()*/ + "]";
	}
	
	public String traitsToString() {
		String abilities = "( ";
		abilities = abilities + traits[0][0] ;							// Startet die Trait Kette
		abilities = abilities + " , " + traits[0][1] ;					// mit dem ersten Trait
		
		for (int i = 1; i < traits.length; i++) {						// automatische Generation des rests
			
				abilities = abilities + " , " + traits[i][0] ;			
				abilities = abilities + " , " + traits[i][1] ;
		}
		abilities = abilities + " )";
		return abilities;
	}
	
}
