package model;

public class ClassTemplates {

	 private final String name[] = {"Barbar","Barde","Druide","K�mpfer", "M�nch","Paladin","Waldl�ufer","Dieb","Trixer","Hexer","Zauberer"};
	 private final int hitdie[] = {12, 8, 8, 8, 10, 8, 10, 10, 8, 6, 8, 6}; //hit die pro level der klassen aufsteigend
	 private final int hpMultiplier[] = {12, 8, 8, 8, 10, 8, 10, 10, 8, 6, 8, 6}; //hp berechnung der klassen aufstaeigend
	 private final String profGear[] = {"Armor: Light armor, medium armor, shields | Weapons: Simple weapons, martial weapons | Tools: None",
	 									"Armor: Light armor | Weapons: Simple weapons, hand crossbows, longswords, rapiers, shortswords | Tools: Three musical instruments of your choice", 
	 									"Armor: Light armor, medium armor, shields | Weapons: Ali simple weapons | Tools: None",
	 									"Armor: Light armor, medium armor, shields (druids will not wear armor or use shields made of metal) | Weapons: Clubs, daggers, darts, javelins, maces, quarlerstaffs, scimitars, sickles, slings. spears | Tools: Herbalism kit",
	 									"Armar: All armar, shields | Weapons: Simple weapons, martial weapons | Tools: None",
	 									"Armor: None | Weapons: Simple weapons, shortswords | Tools: Choose one type of artisan's tools or one musical inslrument",
	 									"Armor: All armor, shields | Weapons: Simple weapons, martial weapons | Tools: None",
	 									"Armor: Light armor, medium armor, shields | Weapons: Simple weapons, martial weapons | Tools: None",
	 									"Armor: Light armor | Weapons: Simple weapons, hand crossbows, longswords, rapiers, shortswords | Tools: Thieves' tools",
	 									"Armar: None | Weapons: Daggers, darts, slings, quarterstaffs, light crossbows | Tools: None",
	 									"Armor: Lighl armor | Weapons: Simple weapons | Tools: None",
	 									"Armor: None | Weapons: Daggers, darts, slings, quarterstaffs, light crossbows | Tools: None"					
	 };
	 									//	Str	 Dex   Con  Int   Wis   Cha   Acr   Ani  Arc   Ath   Dec   His   Ins   Int   Inv   Med  Nat   Perc  Perf  Pers  Rel   Slei  Ste   Sur
	 private boolean[][] proficiencies = {{true,false,true,false,false,false,false,true,false,false,false,false,false,false,true,false,false,false,false,false,false,false,false,false},
			                              {false,true,false,false,false,true,false,false,true,false,true,false,true,false,false,false,false,false,false,false,false,false,false,false},
			                              {false,false,false,false,true,true,false,false,false,false,false,false,false,false,false,true,false,false,false,false,true,false,false,false},
			                              {false,false,false,true,true,false,false,true,false,false,false,false,false,false,false,false,true,false,false,false,false,false,false,false},
			                              {true,false,true,false,false,false,false,false,false,false,false,true,false,false,false,false,true,false,false,false,false,false,false,false},
			                              {true,true,false,false,false,false,true,false,false,true,false,false,false,false,false,false,false,false,false,false,false,false,false,false},
			                              {false,false,false,false,true,true,false,false,false,false,false,false,false,true,false,false,false,false,false,false,true,false,false,false},
			                              {true,true,false,false,false,false,false,true,false,false,false,false,false,false,false,false,true,false,false,false,false,false,false,false},
			                              {false,true,false,true,false,false,false,false,false,false,true,false,false,false,true,false,false,true,false,false,false,true,true,false},
			                              {false,false,true,false,false,true,false,false,true,false,false,false,true,false,false,false,false,false,false,false,false,false,false,false},
			                              {false,false,false,false,true,true,false,false,false,false,true,false,false,false,true,false,false,false,false,false,false,false,false,false},
			                              {false,false,false,true,true,false,false,false,false,false,true,true,false,false,false,false,false,false,false,false,false,false,false,false}                            
			                              };

			 
	 
	 public ClassTemplates() {
		
	 }
	 
	 public String getName(int i) {
		 return name[i];
	 };
	 
	 public int getHitdie(int i) {
		 return hitdie[i];
	 }
	 
	 public int getHpMultiplier(int i) {
		 return hpMultiplier[i];
	 }
	 
	 public String getProfGear(int i) {
		 return profGear[i];
	 }
	 
	 public boolean[] getProficiencies(int i) {
		 
		boolean[] uebergabe = new boolean[24];
		for(int k = 0; i<proficiencies.length; i++) {
			uebergabe[k] = proficiencies[i][k];
		}
			
		 
		 return uebergabe;
	 }
	 
}
