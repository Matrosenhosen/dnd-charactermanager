package model;

import java.util.Arrays;

public class Charakter {
	
private String character_Name;
private String player_Name;
private CharacterClass klasse;
private Race rasse;
private String alignment;
private int xp;
private int hp;
private int str;
private int dex;
private int con;
private int intelli;
private int wis;
private int cha;
private int ac;
private int copper;
//private String[] inventory;
//private int spellslots;

public Charakter(String character_Name, String player_Name, CharacterClass klasse, Race rasse, String alignment, int xp,
		int hp, int str, int dex, int con, int intelli, int wis, int cha, int ac, int copper) {
	super();
	this.character_Name = character_Name;
	this.player_Name = player_Name;
	this.klasse = klasse;
	this.rasse = rasse;
	this.alignment = alignment;
	this.xp = xp;
	this.hp = hp;
	this.str = str;
	this.dex = dex;
	this.con = con;
	this.intelli = intelli;
	this.wis = wis;
	this.cha = cha;
	this.ac = ac;
	this.copper = copper;
}


//---------------------------------------------------Getter-Setter-----------------------------------------------------------------------

public Charakter() {
	// TODO Auto-generated constructor stub
}


public String getCharacter_Name() {
	return character_Name;
}


public void setCharacter_Name(String character_Name) {
	this.character_Name = character_Name;
}


public String getPlayer_Name() {
	return player_Name;
}


public void setPlayer_Name(String player_Name) {
	this.player_Name = player_Name;
}


public CharacterClass getKlasse() {
	return klasse;
}


public void setKlasse(CharacterClass klasse) {
	this.klasse = klasse;
}


public Race getRasse() {
	return rasse;
}


public void setRasse(Race rasse) {
	this.rasse = rasse;
}


public String getAlignment() {
	return alignment;
}


public void setAlignment(String alignment) {
	this.alignment = alignment;
}


public int getXp() {
	return xp;
}


public void setXp(int xp) {
	this.xp = xp;
}


public int getHp() {
	return hp;
}


public void setHp(int hp) {
	this.hp = hp;
}


public int getStr() {
	return str;
}


public void setStr(int str) {
	this.str = str;
}


public int getDex() {
	return dex;
}


public void setDex(int dex) {
	this.dex = dex;
}


public int getCon() {
	return con;
}


public void setCon(int con) {
	this.con = con;
}


public int getInt() {
	return intelli;
}


public void setInt(int i) {
	this.intelli = i;
}


public int getWis() {
	return wis;
}


public void setWis(int wis) {
	this.wis = wis;
}


public int getCha() {
	return cha;
}


public void setCha(int cha) {
	this.cha = cha;
}


public int getAC() {
	return ac;
}


public void setAC(int aC) {
	this.ac = aC;
}


public int getCopper() {
	return copper;
}


public void setCopper(int copper) {
	this.copper = copper;
}

//------------------------------------------------------To-String------------------------------------------------------------------------

@Override
public String toString() {
	return "Character [character_Name=" + character_Name + ", player_Name=" + player_Name + ", klasse=" + klasse
			+ ", rasse=" + rasse + ", alignment=" + alignment + ", xp=" + xp + ", hp=" + hp + ", Str=" + str + ", Dex="
			+ dex + ", Con=" + con + ", Int=" + intelli + ", Wis=" + wis + ", Cha=" + cha + ", AC=" + ac
			+ ", dcSkills_modifier=" + ", copper=" + copper + "]";
}



}