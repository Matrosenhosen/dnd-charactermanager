package model;

	

public class RaceTemplate {
	private final String name[] = {"Zwerg","Elf","Halbling","Mensch","Halbelf","Halb-Ork","Gnom","Drachenblut","Tieflinge"};
	private final int speed[] = {25,30,25,30,30,30,25,30,30};
	private final String abScInName[] = {"Con","Dex","Dex","ALL","Int","Str","Dex","Str","Cha"};
	private final int abScInValue[] = {2,2,2,1,2,2,2,2,2};
	private final String languages[] = {"dwarvish,common","elvish,common","Halfling, common","common", "elvish,common","orkish,common","gnomish,common","draconic,common","abyssal,common"};
	
	public RaceTemplate(){	
	}
	
	public String getName(int i) {
		return name[i];
	}
	public int getSpeed(int i) {
		return speed[i];
	}
	public String getAbScInName(int i) {
		return abScInName[i];
	}
	public int getAbScInValue(int i) {
		return abScInValue[i];
	}
	public String getlLanguages(int i) {
		return languages[i];
	}
	
	
}
