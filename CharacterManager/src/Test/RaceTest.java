package Test;

import java.util.Arrays;

import model.Race;

public class RaceTest {

	public static void main(String[] args) {
		
		
	    String[][] traits = new String[6][2];
	    traits[0][0] = "Darkvision";
	    traits[0][1] = "Accustomed to life underground, you have superior vision in dark and dim conditions. Vou can see in dim light within 60 feet ofyou as if it were bright light, and in darkness as if it were dim lighl. Vou can't discern color in darkness, only shades of gray.";
	    traits[1][0] = "Dwarven Resilience.";
	    traits[1][1] = "Vou have advantage on saving throws against poison, and you have resistance against poison damage (explained in chapter 9).";
	    traits[2][0] = "Dwarven Combat Training";
	    traits[2][1] = "Vou have proficiency with the battleaxe, handaxe, throwing hammer, and warhammer.";
	    traits[3][0] = "TooJProficieney.";
	    traits[3][1] = "Vou gain proficiency with the artisan's tools ofyour choice: smith's tooIs, brewer's supplies, or mason's tools.";
	    traits[4][0] = "Stonecunning";
	    traits[4][1] = "Whenever you make an Intelligence (History) check related to the origin of stonework, you are considered proficient in the History skill and add double your proficiency bonus to the check, instead of your normal proficiency bonus.";
	    traits[5][0] = "Dwarven Toughness.";
	    traits[5][1] = "Your hit point maximum increases by 1,and it increases by 1 every time you gain a leveI";
	    
	    Race test1 = new Race("Dwarf", 25, "Wis", 1, "Common, Dwarvish", traits);
		
		System.out.println(test1.toString());
	}

}
