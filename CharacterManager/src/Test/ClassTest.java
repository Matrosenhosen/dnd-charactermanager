package Test;

import model.CharacterClass;

public class ClassTest {

	public static void main(String[] args) {
		
		String profGear[] = {"Light armor", "medium armor", "shields", "Simple weapons", "martial weapons"};
		boolean proficiencies [] = new boolean[24];
		proficiencies[0] = true;
		proficiencies[2] = true;
		proficiencies[7] = true;
		proficiencies[9] = true;
		String features[][] = {{"UNARMORED DEFENSE","While you are not wearing any armor, your Armor Class equals 10+your Dexterity modifier +your Constitution modifier. Voucan use a shield and still gain this benefit."},{"RECKLESS ATTACK","Starting at 2nd levei, you can throw aside ali concern for defense to attack with fierce desperation. When you make your first attack on your turn, you can decide to attack recklessly. Doing so gives you advantage on melee weapon attack rolls using Strength during this turn, but attack rolls against you have advantage until your next turn"}};
		
		CharacterClass klasse = new CharacterClass(2, 12, 7, 2, profGear, proficiencies, features);
		
		System.out.println(klasse.toString());

	}

}
