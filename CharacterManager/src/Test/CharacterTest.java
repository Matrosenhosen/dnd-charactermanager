package Test;

import model.CharacterClass;
import model.Charakter;
import model.Race;

public class CharacterTest {
	
	public static void main(String[] args) {
//-------------------------------Klasse-wird-erstellt---------------------------------------------------------------			
		String profGear[] = {"Light armor", "medium armor", "shields", "Simple weapons", "martial weapons"};
		boolean proficiencies [] = new boolean[24];
		proficiencies[0] = true;
		proficiencies[2] = true;
		proficiencies[7] = true;
		proficiencies[9] = true;
		String features[][] = {{"UNARMORED DEFENSE","While you are not wearing any armor, your Armor Class equals 10+your Dexterity modifier +your Constitution modifier. Voucan use a shield and still gain this benefit."},{"RECKLESS ATTACK","Starting at 2nd levei, you can throw aside ali concern for defense to attack with fierce desperation. When you make your first attack on your turn, you can decide to attack recklessly. Doing so gives you advantage on melee weapon attack rolls using Strength during this turn, but attack rolls against you have advantage until your next turn"}};
		
		CharacterClass klasse = new CharacterClass(2, 12, 7, 2, profGear, proficiencies, features);
//-------------------------------Rasse-wird-erstellt----------------------------------------------------------------
		String[][] traits = new String[6][2];
	    traits[0][0] = "Darkvision";
	    traits[0][1] = "Accustomed to life underground, you have superior vision in dark and dim conditions. Vou can see in dim light within 60 feet ofyou as if it were bright light, and in darkness as if it were dim lighl. Vou can't discern color in darkness, only shades of gray.";
	    traits[1][0] = "Dwarven Resilience.";
	    traits[1][1] = "Vou have advantage on saving throws against poison, and you have resistance against poison damage (explained in chapter 9).";
	    traits[2][0] = "Dwarven Combat Training";
	    traits[2][1] = "Vou have proficiency with the battleaxe, handaxe, throwing hammer, and warhammer.";
	    traits[3][0] = "TooJProficieney.";
	    traits[3][1] = "Vou gain proficiency with the artisan's tools ofyour choice: smith's tooIs, brewer's supplies, or mason's tools.";
	    traits[4][0] = "Stonecunning";
	    traits[4][1] = "Whenever you make an Intelligence (History) check related to the origin of stonework, you are considered proficient in the History skill and add double your proficiency bonus to the check, instead of your normal proficiency bonus.";
	    traits[5][0] = "Dwarven Toughness.";
	    traits[5][1] = "Your hit point maximum increases by 1,and it increases by 1 every time you gain a leveI";
	    
	    Race rasse = new Race("Dwarf", 25, "Wis", 1, "Common, Dwarvish", traits);
//-------------------------------Character-wird-erstellt------------------------------------------------------------
	    Charakter myCharacter = new Charakter("Borsig", "Presian", klasse, rasse, "neutral good", 2500, 24, 18, 13, 16, 13, 10, 9, 15, 15000);
	
	    System.out.println(myCharacter.toString());
	}
}
