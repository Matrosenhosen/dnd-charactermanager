package db_controll;

import java.sql.*;
import logic.*;
import model.Charakter;

public class DB_Controll {
	
	private Connection con;
	private String ip;
	private String url;
	private String user;
	private String password;

	public DB_Controll(String ip) {
		this.setIp(ip);
	}

	private String getIp() {
		return ip;
	}

	private void setIp(String ip) {
		this.ip = ip;
	}
	
	private void verbindungAufbauen() {
		this.verbindungAufbauen("jdbc:mysql://"+ this.getIp() +"/charaktermanager?");
	}
	
	/**
	 * Methode zum Aufbau einer Verbindung zur Datenbank. Sie benoetigt die Attribute url, user und password und befuellt
	 * das Attribut con
	 */
	private void verbindungAufbauen(String url) {
		try {
			// Parameter fuer Verbindungsaufbau definieren
			String driver = "com.mysql.jdbc.Driver";
			user = "root";
			password = "";
			// JDBC-Treiber laden
			Class.forName(driver);
			// Verbindung aufbauen
			con = DriverManager.getConnection(url, user, password);
		} catch (Exception ex) {
			ex.printStackTrace();// Fehlermeldung ausgeben
		}
	}
	
	public void setup() {
		try {
			url = "jdbc:mysql://"+ this.getIp() +"/?";
			this.verbindungAufbauen(url);

			// SQL-Anweisungen ausfuehren
			Statement stmt;
			stmt = con.createStatement();
			stmt.executeUpdate("CREATE DATABASE IF NOT EXISTS charaktermanager;");

			// Verbindung schliessen
			con.close();
			
			//URL aendern
			url = "jdbc:mysql://"+ this.getIp() +"/charaktermanager?";

			// Verbindung aufbauen
			con = DriverManager.getConnection(url, user, password);
			stmt = con.createStatement();

			//Tabellen erstellen
			stmt.executeUpdate("CREATE TABLE IF NOT EXISTS T_Rassen(" +
					"P_Rassenname VARCHAR(30) PRIMARY KEY," +
					"AbScInName VARCHAR(30)," +
					"AbScInValue INT," +
					"Geschwindigkeit INT," +
					"Sprache VARCHAR(30)," +
					"Traitname VARCHAR(30)," +
					"Traitbeschreibung VARCHAR(500)" +
					");");
			stmt.executeUpdate("CREATE TABLE IF NOT EXISTS T_Klassen( " +
					"P_Klassenname VARCHAR(30) PRIMARY KEY, " +
					"HitDie INT," +
					"ProfGear VARCHAR(500)," +
					"Proficiencys VARCHAR(500)," +
					"FeatureName VARCHAR(30)," +
					"FeatureBeschreibung VARCHAR(500)" +
					");");
			stmt.executeUpdate("CREATE TABLE IF NOT EXISTS T_Skills(" +
					"P_Skillname VARCHAR(60) PRIMARY KEY" +
					");");
			stmt.executeUpdate("CREATE TABLE IF NOT EXISTS T_KlassenSKills(" +
					"P_F_Klassenname VARCHAR(30)," +
					"P_F_Skillname VARCHAR(60)," +
					"PRIMARY KEY(P_F_Klassenname, P_F_Skillname)," +
					"FOREIGN KEY(P_F_Klassenname) REFERENCES T_Klassen(P_Klassenname)," + 
					"FOREIGN KEY(P_F_Skillname) REFERENCES T_Skills(P_Skillname)" +
					");");
			stmt.executeUpdate("CREATE TABLE IF NOT EXISTS T_Charakter(" +
					"P_CharakterID INT UNSIGNED AUTO_INCREMENT PRIMARY KEY," +
					"CharakterName VARCHAR(30)," +
					"PlayerName VARCHAR(30)," +
					"Kupfer INT," +
					"HP INT," +
					"XP INT," +
					"Str INT," +
					"Dex INT," + 
					"Con INT," +
					"Intelligenz INT," +
					"Wis INT," +
					"Cha INT," +
					"F_Rassenname VARCHAR(30)," +
					"F_Klassenname VARCHAR(30)," +
					"FOREIGN KEY(F_Rassenname) REFERENCES T_Rassen(P_Rassenname)," +
					"FOREIGN KEY(F_Klassenname) REFERENCES T_Klassen(P_Klassenname)" +
					");");
			stmt.executeUpdate("CREATE TABLE IF NOT EXISTS T_Items(" + 
					"P_ItemID INT UNSIGNED AUTO_INCREMENT PRIMARY KEY," + 
					"Name VARCHAR(30)," + 
					"Modifier CHAR(3)," + 
					"Wert INT," + 
					"Beschreibung VARCHAR(500)" + 
					");");
			stmt.executeUpdate("CREATE TABLE IF NOT EXISTS T_Sprachen(" +
					"P_Sprache VARCHAR(30) PRIMARY KEY," +
					"Schriftzeichen VARCHAR(30)" +
					");");
			stmt.executeUpdate("CREATE TABLE IF NOT EXISTS T_CharakterItems(" +
					"P_F_CharakterID INT UNSIGNED," +
					"P_F_ItemID INT UNSIGNED," +
					"PRIMARY KEY(P_F_CharakterID, P_F_ItemID),"+
					"FOREIGN KEY(P_F_CharakterID) REFERENCES T_Charakter(P_CharakterID)," + 
					"FOREIGN KEY(P_F_ItemID) REFERENCES T_Items(P_ItemID)"+
					");");
			stmt.executeUpdate("CREATE TABLE IF NOT EXISTS T_CharakterSprachen(" +
					"P_F_CharakterID INT UNSIGNED," +
					"P_F_Sprache VARCHAR(30)," +
					"PRIMARY KEY(P_F_CharakterID, P_F_Sprache)," +
					"FOREIGN KEY(P_F_CharakterID) REFERENCES T_Charakter(P_CharakterID)," +
					"FOREIGN KEY(P_F_Sprache) REFERENCES T_Sprachen(P_Sprache)" +
					");");
			
			ResultSet rs = stmt.executeQuery("SELECT P_Rassenname FROM T_Rassen;");
			if(!rs.next()) {
					stmt.executeUpdate("INSERT INTO T_Rassen(P_Rassenname) VALUES ('Zwerg');");
					stmt.executeUpdate("INSERT INTO T_Rassen(P_Rassenname) VALUES ('Elf');");
					stmt.executeUpdate("INSERT INTO T_Rassen(P_Rassenname) VALUES ('Halbling');");
					stmt.executeUpdate("INSERT INTO T_Rassen(P_Rassenname) VALUES ('Mensch');");
					stmt.executeUpdate("INSERT INTO T_Rassen(P_Rassenname) VALUES ('Halbelf');");
					stmt.executeUpdate("INSERT INTO T_Rassen(P_Rassenname) VALUES ('Halb-Ork');");
					stmt.executeUpdate("INSERT INTO T_Rassen(P_Rassenname) VALUES ('Gnom');");
					stmt.executeUpdate("INSERT INTO T_Rassen(P_Rassenname) VALUES ('Drachenblut');");
					stmt.executeUpdate("INSERT INTO T_Rassen(P_Rassenname) VALUES ('Tiefling');");
			
					stmt.executeUpdate("INSERT INTO T_Klassen(P_Klassenname) VALUES ('Barbar');");
					stmt.executeUpdate("INSERT INTO T_Klassen(P_Klassenname) VALUES ('Barde');");
					stmt.executeUpdate("INSERT INTO T_Klassen(P_Klassenname) VALUES ('Druide');");
					stmt.executeUpdate("INSERT INTO T_Klassen(P_Klassenname) VALUES ('K�mpfer');");
					stmt.executeUpdate("INSERT INTO T_Klassen(P_Klassenname) VALUES ('M�nch');");
					stmt.executeUpdate("INSERT INTO T_Klassen(P_Klassenname) VALUES ('Paladin');");
					stmt.executeUpdate("INSERT INTO T_Klassen(P_Klassenname) VALUES ('Waldl�ufer');");
					stmt.executeUpdate("INSERT INTO T_Klassen(P_Klassenname) VALUES ('Dieb');");
					stmt.executeUpdate("INSERT INTO T_Klassen(P_Klassenname) VALUES ('Zauberer');");
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
	}
	
	public void charakterEinfuegen(Charakter charakter) {
		
		this.verbindungAufbauen();
		
		try {
			String sql = "INSERT INTO T_Charakter(CharakterName, PlayerName, Kupfer, HP, XP, Str, Dex, Con, Intelligenz, Wis, Cha," +
						 "F_Rassenname, F_Klassenname) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
			
			PreparedStatement pStmt =  con.prepareStatement(sql);
			
			pStmt.setString(1, charakter.getCharacter_Name());
			pStmt.setString(2, charakter.getPlayer_Name());
			pStmt.setInt(3, charakter.getCopper()); 
			pStmt.setInt(4, charakter.getHp()); 
			pStmt.setInt(5, charakter.getXp()); 
			pStmt.setInt(6, charakter.getStr());
			pStmt.setInt(7, charakter.getDex());
			pStmt.setInt(8, charakter.getCon());
			pStmt.setInt(9, charakter.getInt());
			pStmt.setInt(10, charakter.getWis());
			pStmt.setInt(11, charakter.getCha());
			pStmt.setString(12, charakter.getRasse().getName());
			pStmt.setString(13, charakter.getKlasse().getName());
			
			pStmt.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

/*	public Charakter charakterAbrufen(String name) {
		Charakter charakter = null;
		this.verbindungAufbauen();
		
		try {
			String sql = "SELECT * " + 
					"FROM T_Charakter" + 
					"WHERE CharakterName LIKE ?;";
			
			PreparedStatement pStmt = con.prepareStatement(sql);
			
			pStmt.setString(1, name);
			
			ResultSet rs = pStmt.executeQuery();
			rs.next();
			
			String player = rs.getString(2);
			int kupfer = rs.getInt(3);
			int hp = rs.getInt(4);
			int xp = rs.getInt(5);
			int str = rs.getInt(6);
			int dex = rs.getInt(7);
			int con = rs.getInt(8);
			int intelli = rs.getInt(9);
			int wis = rs.getInt(10);
			int cha = rs.getInt(11);
			String rassenname = rs.getString(12);
			String klassenname = rs.getString(13);
			
			charakter = new Charakter(name, player, new CharacterClass(klassenname, lvl, hitdie, hpMultiplier, profMod, profGear, proficiencys, features), rasse, alignment, xp, hp, str, dex, con, intelli, wis, cha, ac, copper)
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return charakte;
	}*/

}
