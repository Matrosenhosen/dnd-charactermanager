package logic;

import java.util.Random;

import db_controll.DB_Controll;
import model.CharacterClass;
import model.Charakter;
import model.ClassTemplates;
import model.Race;
import model.RaceTemplate;

public class Controller {
	
	private final int SUCCESS = 1;
	private final int FAILIURE = -1;
	private Charakter charakter;
	private DB_Controll db = new DB_Controll("127.0.0.1");

	public Controller(Charakter charakter) {
		this.charakter = charakter;
	}

	// --------------------Character-Creation------------------------------------------

	// public int[] determineStats () {
	// int[] stats = new int[6];
	// for(int i= 0; i<6; i++) {
	// int stat = (int) Math.random()+6;
	// stats[i] = stat;
	// }
	//
	//
	// return null;
	// }
	public static int randomStat() {
		int[] stats = new int[4];
		int small = 0;
		Random r = new Random();
		for (int i = 0; i < stats.length; i++) {
			stats[i] = r.nextInt(6) + 1;
		}
		for (int i = 0; i < stats.length - 1; i++) {
			if (stats[i + 1] < stats[small])
				small = i + 1;
		}
		stats[small] = 0;
		int stat = 0;
		for (int i = 0; i < stats.length; i++) {
			stat += stats[i];
		}
		return stat;
	}

	// --------------------Character-Management----------------------------------------

	public int heal(int amount) {
		if (amount > 0) {
			charakter.setHp(charakter.getHp() + amount);
			return SUCCESS;
		}
		return FAILIURE;
	}

	public int damage(int amount) {
		if (amount > 0) {
			charakter.setHp(charakter.getHp() - amount);
			return SUCCESS;
		}
		return FAILIURE;
	}
	
	public void generateCharacter(String playername, String charactername, int klasse, int race, int str, int dex, int con, int Int, int wis, int cha,String alignment) {
		RaceTemplate templateRa = new RaceTemplate();
		String[][] traits = {{"nicht implementiert"},{"nicht implementiert"}};
		Race newRace = new Race(templateRa.getName(race),templateRa.getSpeed(race),templateRa.getAbScInName(race),templateRa.getAbScInValue(race),templateRa.getlLanguages(race),traits);
		
		ClassTemplates templateCl = new ClassTemplates();
		String[][] features = {{"nicht implementiert"},{"nicht implementiert"}};
		CharacterClass newClass = new CharacterClass(templateCl.getName(klasse),1,templateCl.getHitdie(klasse),templateCl.getHpMultiplier(klasse),2,templateCl.getProfGear(klasse),templateCl.getProficiencies(klasse),features);
		
		String r = newRace.getAbScInName();
		switch (r) {
		case "Str":
			str = str + newRace.getAbScInValue();
			break;
		case "Dex":
			dex = dex + newRace.getAbScInValue();
			break;
		case "Con":
			con = con + newRace.getAbScInValue();
			break;
		case "Int":
			Int = Int + newRace.getAbScInValue();
			break;
		case "Wis":
			wis = wis + newRace.getAbScInValue();
			break;
		case "Cha":
			cha = cha + newRace.getAbScInValue();
			break;
		case "ALL":
			str = str + newRace.getAbScInValue();
			dex = dex + newRace.getAbScInValue();
			con = con + newRace.getAbScInValue();
			Int = Int + newRace.getAbScInValue();
			wis = wis + newRace.getAbScInValue();
			cha = cha + newRace.getAbScInValue();
			break;
		default:
			break;
		}
		
		int ac = 10 +  Math.round((dex-10)/2);
		
		Charakter newCharakter = new Charakter(charactername,playername,newClass,newRace,alignment,0,newClass.getHpMultiplier(),str,dex,con,Int,wis,cha,ac,0);
		this.charakter = newCharakter;
	}
	
	/**
	 * DB Methode charakterEinf�gen aufrufen
	 */
	
	public void charakterEinfuegen() {
		db.charakterEinfuegen(charakter);
	}
	

}
